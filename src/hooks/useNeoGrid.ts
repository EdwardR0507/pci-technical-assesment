import { useCallback, useEffect, useMemo, useRef, useState } from "react";

import { ColDef } from "ag-grid-community";
import { AgGridReact } from "ag-grid-react";

import { columnDefs } from "../components/NeoGrid/columns";
import { NearEarthAsteroids } from "../interfaces";
import { fromJsonToExcelCells, neoColumn } from "../utils";

export const useNeoGrid = () => {
  const gridRef = useRef<AgGridReact<NearEarthAsteroids>>(null);

  const [colDefs, setColDefs] = useState<ColDef[]>(columnDefs);

  const defaultColDef = useMemo<ColDef>(() => {
    return {
      filter: true,
      sortable: true,
    };
  }, []);

  const copyToClipboard = useCallback(
    (event: KeyboardEvent) => {
      if (event.ctrlKey && (event.key === "c" || event.key === "C")) {
        const selectedNodesData = gridRef
          .current!.api.getSelectedNodes()
          .map((node) => node.data);

        const excelCells = fromJsonToExcelCells(selectedNodesData);

        navigator.clipboard.writeText(excelCells);
      }
    },
    [gridRef]
  );

  const handleClearFiltersAndSorters = useCallback(() => {
    gridRef.current!.api.setFilterModel(null);
    gridRef.current!.columnApi.applyColumnState({
      defaultState: { sort: null },
    });
  }, []);

  const onGridReady = useCallback(() => {
    setColDefs(columnDefs.map(neoColumn));
  }, []);

  useEffect(() => {
    window.addEventListener("keydown", copyToClipboard);

    return () => window.removeEventListener("keydown", copyToClipboard);
  }, [copyToClipboard]);

  return {
    gridRef,
    handleClearFiltersAndSorters,
    defaultColDef,
    colDefs,
    onGridReady,
  };
};
