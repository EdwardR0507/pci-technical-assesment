import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import { AgGridReact } from "ag-grid-react";

import { useNeoGrid } from "../../hooks/useNeoGrid";
import data from "../../near-earth-asteroids.json";
import { neoColumn } from "../../utils";
import { gridOptions } from "./columns";
import "./styles.css";

const NeoGrid = (): JSX.Element => {
  const {
    colDefs,
    defaultColDef,
    gridRef,
    handleClearFiltersAndSorters,
    onGridReady,
  } = useNeoGrid();

  return (
    <div className="ag-theme-alpine" style={{ height: 900, width: 1920 }}>
      <section className="header-container">
        <h1 className="header__title">Near-Earth Object Overview</h1>
        <button
          className="header__button"
          onClick={handleClearFiltersAndSorters}
        >
          Clear Filters and Sorters
        </button>
      </section>
      <AgGridReact
        columnDefs={colDefs.map(neoColumn)}
        gridOptions={gridOptions}
        defaultColDef={defaultColDef}
        onGridReady={onGridReady}
        ref={gridRef}
        rowData={data}
        rowGroupPanelShow={"always"}
        rowSelection="multiple"
      />
    </div>
  );
};

export default NeoGrid;
