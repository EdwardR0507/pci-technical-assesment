import { ColDef } from "ag-grid-community";

import { NearEarthAsteroids } from "../../interfaces";

const columnTypes = {
  string: { filter: "agTextColumnFilter" },
  number: { filter: "agNumberColumnFilter" },
  date: { filter: "agDateColumnFilter" },
};

export const columnDefs: ColDef<NearEarthAsteroids>[] = [
  { field: "designation", headerName: "Designation", type: "string" },
  {
    field: "discovery_date",
    headerName: "Discovery Date",
    type: "date",
  },
  { field: "h_mag", headerName: "H (mag)", type: "number" },
  { field: "moid_au", headerName: "MOID (au)", type: "number" },
  { field: "q_au_1", headerName: "q (au)", type: "number" },
  { field: "q_au_2", headerName: "Q (au)", type: "number" },
  { field: "period_yr", headerName: "Period (yr)", type: "number" },
  { field: "i_deg", headerName: "Inclination (deg)", type: "number" },
  {
    field: "pha",
    headerName: "Potentially Hazardous",
    type: "string",
  },
  {
    field: "orbit_class",
    headerName: "Orbit Class",
    type: "string",
  },
];

export const gridOptions = {
  columnTypes: columnTypes,
  columnDefs: columnDefs,
};
