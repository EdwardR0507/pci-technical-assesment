import { ColDef } from "ag-grid-community";

import { NearEarthAsteroids } from "../interfaces";

const dateValueFormatter = (params: { value: string }) => {
  return new Date(params.value).toLocaleDateString("en-US", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
};

const phaValueFormatter = (params: { value: string }) => {
  const valueMap: { [key: string]: string } = {
    Y: "Yes",
    N: "No",
    "n/a": "",
  };
  return valueMap[params.value];
};

export const neoColumn = (column: ColDef<NearEarthAsteroids>) => {
  switch (column.field) {
    case "pha":
      column.valueFormatter = phaValueFormatter;
      break;
    case "discovery_date":
      column.valueFormatter = dateValueFormatter;
      break;
    default:
      break;
  }
  return addFilterAndComparator(column);
};

const addFilterAndComparator = (column: ColDef<NearEarthAsteroids>) => {
  switch (column.type) {
    case "number":
      column.comparator = (valueA: any, valueB: any) => valueA - valueB;
      column.filter = "agNumberColumnFilter";
      break;
    case "date":
      column.comparator = (valueA: any, valueB: any) =>
        new Date(valueA).getTime() - new Date(valueB).getTime();
      column.filter = "agTextColumnFilter";
      break;
    default:
      column.filter = "agTextColumnFilter";
      break;
  }
  return column;
};

export const fromJsonToExcelCells = (
  data: (NearEarthAsteroids | undefined)[]
) => {
  const formattedData = data.map((row) => {
    if (!row) return row;

    const formattedRow: Record<string, string> = {};
    for (const [key, value] of Object.entries(row)) {
      switch (key) {
        case "pha":
          formattedRow[key] = phaValueFormatter({ value: value as string });
          break;
        case "discovery_date":
          formattedRow[key] = dateValueFormatter({ value: value as string });
          break;
        default:
          formattedRow[key] = value as string;
          break;
      }
    }
    return formattedRow;
  });

  const rows = formattedData.map((row) =>
    Object.values(row as Record<string, string>)
  );

  return rows.map((row) => row.join("\t")).join("\r");
};
